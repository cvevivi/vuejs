import { createRouter, createWebHistory } from "vue-router";

import Main from "../views/MainMain.vue";
import Allcities from "../views/MainAllcities.vue";
import News from "../views/MainNews.vue";
import Records from "../views/MainRecords.vue";
import NewsAbout from "../views/NewsAbout.vue";

const routes = [
    {
        path: "/weather",
        component: Allcities,
        name: "weather"
    },
    {
        path: "/weather/:city/",
        component: Allcities,
        name: "cityWeather"
    },
    {
        path: "/",
        component: Main,
        name: "main"
    },
    {
        path: "/news",
        component: News,
        name: "news"
    },
    {
        path: "/news/:id/",
        component: NewsAbout,
        name: "news-item"
    },
    {
        path: "/records",
        component: Records,
        name: "records"
    }
]
const router = createRouter({
    history : createWebHistory(process.env.BASE_URL),
    routes
});
export default router;