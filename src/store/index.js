import { createStore } from "vuex";

const store = createStore({
    state(){
        return {
            api_token:"76af8574a48968c8ab3c3a24168e6dcf",
            cities: [
                {
                    name: "Москва", title: "Moscow", currentweather: {}, forecast: []
                },
                {
                    name: "Магадан", title: "Magadan", currentweather: {}, forecast: []
                },
                {
                    name: "Екатеринбург", title: "Ekaterinburg", currentweather: {}, forecast: []
                },
            ],
            news : [],
            currentCity : false
        }
    },

    actions:{
        async getWeatherByCity(ctx, city){
            const res = await fetch (`https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${ctx.state.api_token}`)
            if(res.ok){
                let result = await res.json();
                ctx.commit("setweather", {city, weather : result});
                return true;
            }
            return false;

        },
        async getWeather(ctx){
            ctx.state.cities.forEach(city => {
                ctx.dispatch("getWeatherByCity", city.title).then(response => {
                    if(response){
                        console.log(`Погода города ${city.name} обновлена`);
                    }
                    else{
                        console.log(`Произошла ошибка (${city.name})`);
                    }
                })
            })
            return true;
        },
        async getNews(ctx){
            const res = await fetch('http://localhost:8080/main.json');
            const result = await res.json();
            ctx.commit("setNews", result);
        }
    },
    mutations:{
        setweather (state, data){
            let cityIndex = state.cities.findIndex((city) => city.title == data.city);
            if(cityIndex != -1){
                state.cities[cityIndex].currentweather = data.weather;
                console.log(state.cities);
            }
        },
        setCurrentCity(state, city){
            state.currentCity = city;
        },
        addCity(state, data){
            let cityIndex = state.cities.findIndex((city) => city.name == data.rus);
            if(cityIndex == -1){
                state.cities.push({
                    name : data.rus,
                    title : data.eng,
                    currentweather : {},
                    forecast : []
                })
            }
        },
        setNews(state, data){
            state.news = data;
        }
    },
    getters:{
        getCity(state){
            if(state.currentCity){
                return state.cities.find(city => city.title == state.currentCity);
            }
        },
        getNews: (state) => (id) => {
            console.log(state.news[Number(id)]);
            return state.news[Number(id)];
        }
    },
});

export default store;